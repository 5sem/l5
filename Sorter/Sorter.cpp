#include "Sorter.h"
#include <vector>
#include <fstream>
#include <stdexcept>
#include "Pagination.h"
#include "SortTask.h"
#include <algorithm>

Sorter::~Sorter()
{
	delete processor;
}

void Sorter::StartSorting()
{
	hInputThread = CreateThread(NULL, 0, InputThreadFunction, this, 0, NULL);
	hOutputThread = CreateThread(NULL, 0, OutputThreadFunction, this, CREATE_SUSPENDED, NULL);
}

std::vector<std::string> Sorter::ReadFile(const std::string& fileName)
{
	std::ifstream input(fileName);
	std::vector<std::string> result;

	if (!input)
	{
		throw std::runtime_error("can't open file");
	}

	std::string line;
	while (std::getline(input, line))
	{
		if (!line.empty())
		{
			result.push_back(move(line));
		}
	}

	return result;
}

DWORD WINAPI Sorter::InputThreadFunction(LPVOID lpParam)
{
	auto sorter = static_cast<Sorter*>(lpParam);

	sorter->lines = Sorter::ReadFile(sorter->fileName);

	Queue<shared_ptr<ITask>> queue;

	for (const auto& part : Paginate(sorter->lines, (int)ceil(sorter->lines.size() * 1.0 / sorter->threadsCount)))
	{
		queue.Enqueue(make_shared<SortTask>(part));
	}

	sorter->processor = new MultithreadedProcessor(move(queue), sorter->threadsCount);
	sorter->processor->StartProcessing();

	ResumeThread(sorter->hOutputThread);

	return 0;
}

DWORD WINAPI Sorter::OutputThreadFunction(LPVOID lpParam)
{
	auto sorter = static_cast<Sorter*>(lpParam);

	WaitForSingleObject(sorter->hInputThread, INFINITE);
	CloseHandle(sorter->hInputThread);

	sorter->processor->Wait();

	for (const auto& part : Paginate(sorter->lines, (int)ceil(sorter->lines.size() * 1.0 / sorter->threadsCount)))
	{
		std::inplace_merge(sorter->lines.begin(), part.begin(), part.end());
	}

	WriteFile(sorter->fileName, sorter->lines);
	
	return 0;
}

void Sorter::WriteFile(const std::string& fileName, const std::vector<std::string>& lines)
{
	std::ofstream output(fileName);

	if (!output)
	{
		throw std::runtime_error("can't open file");
	}

	for (auto line : lines)
	{
		output << line << std::endl;
	}
}

void Sorter::Wait()
{
	WaitForSingleObject(hOutputThread, INFINITE);
	CloseHandle(hOutputThread);
}
