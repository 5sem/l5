#pragma once
#include <string>
#include <vector>
#include "../Task/MultithreadedProcessor.h"
#include <Windows.h>

class Sorter
{
public:
	explicit Sorter(const std::string& fileName) : fileName(fileName) {};
	Sorter(const std::string& fileName, int maxThreadsCount) : fileName(fileName), threadsCount(maxThreadsCount) {};
	~Sorter();
	void StartSorting();
	void Wait();
private:
	static std::vector<std::string> ReadFile(const std::string& fileName);
	static DWORD WINAPI InputThreadFunction(LPVOID lpParam);
	static DWORD WINAPI OutputThreadFunction(LPVOID lpParam);
	static void WriteFile(const std::string& fileName, const std::vector<std::string>& lines);

	std::string fileName;
	std::vector<std::string> lines;
	MultithreadedProcessor* processor = NULL;
	HANDLE hOutputThread = NULL;
	HANDLE hInputThread = NULL;
	int threadsCount = 8;
};

