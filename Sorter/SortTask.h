#pragma once
#include "../Task/Task.h"
#include <string>
#include <vector>
#include "Pagination.h"

class SortTask :
    public ITask
{
public:
    explicit SortTask(const IteratorRange<std::vector<std::string>::iterator>& range)
        : range(range) {};
    virtual void Execute() override;
private:
    IteratorRange<std::vector<std::string>::iterator> range;
};