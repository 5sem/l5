#pragma once
#include <numeric>
#include <iostream>
#include <vector>
#include <string>
using namespace std;

template <typename Iterator>
class IteratorRange {
public:
	IteratorRange(Iterator begin, Iterator end)
	{
		_begin = begin;
		_end = end;
	}

	Iterator begin() const { return _begin; }
	Iterator end() const { return _end; }

	size_t size() const { return _end - _begin; }

private:
	Iterator _begin;
	Iterator _end;
};

template <typename Iterator>
IteratorRange<Iterator> MakePage(Iterator begin, Iterator end)
{
	return IteratorRange<Iterator>{ begin, end };
}

template <typename Iterator>
class Paginator {
public:
	Paginator(Iterator begin, Iterator end, size_t page_size)
	{
		size_t in_page_count = 0;
		auto last_begin = begin;
		auto cur = begin;
		while (cur != end)
		{
			++in_page_count;
			++cur;
			if (in_page_count == page_size || cur == end)
			{
				_pages.push_back(MakePage(last_begin, cur));
				in_page_count = 0;
				last_begin = cur;
			}
		}
	}
	auto begin() const { return _pages.begin(); }
	auto end() const { return _pages.end(); }
	size_t size() const { return _pages.size(); }
private:
	vector<IteratorRange<Iterator>> _pages;
};

template <typename C>
auto Paginate(C& c, size_t page_size) {
	return Paginator{ begin(c), end(c), page_size };
}