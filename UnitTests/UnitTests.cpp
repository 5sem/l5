﻿#include "CppUnitTest.h"
#include "../Queue/Queue.h"
#include "../Sorter/Sorter.h"
#include <fstream>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTests
{
	void CreateTestFile(const std::string& fileName)
	{
		ofstream output(fileName, std::ios_base::out);

		if (!output)
		{
			Assert::Fail();
		}

		for (int i = 10000; i >= 0; --i)
		{
			output << i << std::endl;
		}

		output.close();
	}

	TEST_CLASS(SingleThreadSorter)
	{
		TEST_CLASS_INITIALIZE(SingleSorterInitialization)
		{
			CreateTestFile(fileName);
		}

		TEST_METHOD(TestSorterSingleThreaded)
		{
			Sorter sorter(fileName, 1);
			sorter.StartSorting();
			sorter.Wait();
		}

		static const inline std::string fileName = "D:/largeFileForSingleThreaded.txt";
	};

	TEST_CLASS(MultiThreadSorter)
	{
		TEST_CLASS_INITIALIZE(MultiSorterInitialization)
		{
			CreateTestFile(fileName);
		}

		TEST_METHOD(TestSorterMultiThreaded)
		{
			Sorter sorter(fileName, 8);
			sorter.StartSorting();
			sorter.Wait();
		}

		static const inline std::string fileName = "D:/largeFileForMultiThreaded.txt";
	};

	TEST_CLASS(UnitTests)
	{
	public:

		TEST_METHOD(TestQueue)
		{
			Queue<int> q;

			Assert::IsTrue(q.IsEmpty(), L"queue is not empty");

			for (int i = 0; i < 10; ++i)
			{
				q.Enqueue(i);
			}

			Assert::IsFalse(q.IsEmpty(), L"queue is empty");

			for (int i = 0; i < 10; ++i)
			{
				int item = q.Dequeue();
				Assert::AreEqual(i, item, L"invalid item in queue");
			}
		}

		TEST_METHOD(TestQueueMultithreaded)
		{
			Queue<int> q;
			HANDLE hThreads[ThreadsCount];

			for (int i = 0; i < ThreadsCount; ++i)
			{
				hThreads[i] = CreateThread(
					NULL, // default security attributes
					0, // default stack size
					&TestQueueThreadFunction,
					&q,
					0,
					NULL);
			}

			// Pass control to other threads to fill the queue
			Sleep(1);

			Assert::IsFalse(q.IsEmpty(), L"queue is empty");

			WaitForMultipleObjects(ThreadsCount, hThreads, TRUE, INFINITE);

			Assert::IsTrue(q.IsEmpty(), L"queue is not empty");

			for (int i = 0; i < ThreadsCount; ++i)
			{
				CloseHandle(hThreads[i]);
			}
		}

		static DWORD WINAPI TestQueueThreadFunction(LPVOID lpParam)
		{
			auto q = static_cast<Queue<int>*>(lpParam);
			for (int i = 0; i < QueuedItemsCount; ++i)
			{
				q->Enqueue(i);
			}

			Sleep(10);

			for (int i = 0; i < QueuedItemsCount; ++i)
			{
				q->Dequeue();
			}

			return 0;
		}

		TEST_METHOD(TestSorter)
		{
			Sorter sorter("D:/in.txt");
			sorter.StartSorting();
			sorter.Wait();
		}

	private:
		static const int QueuedItemsCount = 1000;
		static const int ThreadsCount = 5;
	};
}
