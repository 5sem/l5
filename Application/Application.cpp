﻿#include <iostream>
#include <string>
#include "../Sorter/Sorter.h"

int main()
{
    std::cout << "Enter file name: ";
    std::string fileName;

    std::cin >> fileName;

    Sorter sorter(fileName);
    sorter.StartSorting();
    sorter.Wait();

    std::cout << "Done.";
}
