#pragma once

template <typename T>
class Node
{
public:
	Node() : isNull(true) {};
	explicit Node(T value) : value(value), isNull(false) {};

	T GetValue() { return value; }
	void SetValue(T newValue) { value = newValue; }

	Node* GetNextNode() { return next; }
	void SetNextNode(Node* newNext)
	{
		next = newNext;
	}

	Node* GetPreviousNode() { return previous; }
	void SetPreviousNode(Node* newPrevious)
	{
		previous = newPrevious;
	}

	bool IsNull() { return isNull; }

private:
	T value;
	Node* previous;
	Node* next;
	bool isNull;
};