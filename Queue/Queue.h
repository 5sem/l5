#pragma once
#include <Windows.h>
#include "Node.h"
#include <stdexcept>

using namespace std;

template <typename T>
class Queue
{
public:
	Queue();
	Queue(Queue<T>&& other) noexcept;
	void Enqueue(T value);
	T Dequeue();
	bool IsEmpty();
	~Queue();
private:
	Node<T>* head;
	Node<T>* tail;

	CRITICAL_SECTION criticalSection;
};

template<typename T>
inline Queue<T>::Queue()
{
	if (!InitializeCriticalSectionAndSpinCount(&criticalSection, 0x1000))
	{
		throw new runtime_error("can't initialize critical section");
	}

	head = new Node<T>();
	tail = new Node<T>();
	head->SetNextNode(tail);
	tail->SetPreviousNode(tail);
}

template<typename T>
inline Queue<T>::Queue(Queue<T>&& other) noexcept
	: head(other.head), tail(other.tail)
{
	EnterCriticalSection(&other.criticalSection);
	other.head = other.tail = NULL;
	LeaveCriticalSection(&other.criticalSection);

	InitializeCriticalSectionAndSpinCount(&criticalSection, 0x1000);
}

template<typename T>
inline void Queue<T>::Enqueue(T value)
{
	Node<T>* node = new Node<T>(value);

	EnterCriticalSection(&criticalSection);

	node->SetNextNode(head->GetNextNode());
	node->SetPreviousNode(head);
	head->GetNextNode()->SetPreviousNode(node);
	head->SetNextNode(node);

	LeaveCriticalSection(&criticalSection);
}

template<typename T>
inline T Queue<T>::Dequeue()
{
	if (IsEmpty())
	{
		throw new runtime_error("invalid operation. queue is empty");
	}

	EnterCriticalSection(&criticalSection);

	Node<T>* node = tail->GetPreviousNode();
	T result = node->GetValue();

	node->GetPreviousNode()->SetNextNode(tail);
	tail->SetPreviousNode(node->GetPreviousNode());

	LeaveCriticalSection(&criticalSection);

	delete node;

	return result;
}

template<typename T>
inline bool Queue<T>::IsEmpty()
{
	bool result;

	EnterCriticalSection(&criticalSection);

	result = tail->GetPreviousNode()->IsNull();

	LeaveCriticalSection(&criticalSection);

	return result;
}

template<typename T>
inline Queue<T>::~Queue()
{
	if (head != NULL)
	{
		EnterCriticalSection(&criticalSection);
		if (head != NULL)
		{
			while (!IsEmpty())
			{
				Dequeue();
			}

			delete head;

			delete tail;
		}
		LeaveCriticalSection(&criticalSection);
	}

	DeleteCriticalSection(&criticalSection);
}
