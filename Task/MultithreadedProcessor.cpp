#include "MultithreadedProcessor.h"

MultithreadedProcessor::MultithreadedProcessor(Queue<shared_ptr<ITask>> tasks, int maxThreadsCount)
	: tasks(move(tasks)), threads(maxThreadsCount) {}

void MultithreadedProcessor::StartProcessing()
{
	for (auto& hThread : threads)
	{
		InterlockedIncrement(reinterpret_cast<volatile long*>(&threadsCount));
		hThread = CreateThread(
			NULL,
			0,
			&ProcessFunction,
			this,
			0,
			NULL
		);
	}
}

void MultithreadedProcessor::Wait() const
{
	while (threadsCount > 0)
	{
		Yield();
	}

	for (auto hThread : threads)
	{
		CloseHandle(hThread);
	}
}

DWORD __stdcall MultithreadedProcessor::ProcessFunction(LPVOID lpParam)
{
	auto processor = static_cast<MultithreadedProcessor*>(lpParam);

	auto& tasks = processor->tasks;
	while (!tasks.IsEmpty())
	{
		auto task = tasks.Dequeue();

		task->Execute();
	}

	InterlockedDecrement(reinterpret_cast<volatile long*>(&(processor->threadsCount)));

	return 0;
}