#pragma once
#include "Task.h"
#include "../Queue/Queue.h"
#include <memory>
#include <vector>
#include <Windows.h>

class MultithreadedProcessor
{
public:
	MultithreadedProcessor(Queue<std::shared_ptr<ITask>> tasks, int maxThreadsCount);
	void StartProcessing();
	void Wait() const;

private:
	static DWORD WINAPI ProcessFunction(LPVOID lpParam);
	Queue<shared_ptr<ITask>> tasks;
	std::vector<HANDLE> threads;
	int threadsCount = 0;
};